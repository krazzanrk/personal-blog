from django.urls import path
from .views import *

app_name = 'blog'

urlpatterns = [
    path('', HomePage.as_view(), name='index'),
    path('blog', BlogView.as_view(), name='blogs'),
    path('blog/<str:singleblog_slug>', SinglePostView.as_view(), name='single_blog'),
    path('search', SearchView.as_view(), name='search'),
    path('gallery',GalleryView.as_view(),name='gallery'),


]
