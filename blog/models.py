from django.db import models
from ckeditor.fields import RichTextField
from sorl.thumbnail import ImageField
from autoslug import AutoSlugField


# from django.contrib.auth.models import User


# Create your models here.


class CategoryWiseBlog(models.Model):
    title = models.CharField(max_length=50)
    slug = AutoSlugField(populate_from='title', unique=True)

    def __str__(self):
        return self.title


class BlogPost(models.Model):
    title = models.CharField(max_length=50)
    content = RichTextField()
    Pub_date = models.DateTimeField(auto_now=True)
    author = models.CharField(max_length=50)
    slug = AutoSlugField(populate_from='title', unique=True)
    category = models.ForeignKey(CategoryWiseBlog, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def image(self):
        return self.bloghasimage_set.first()


class BlogHasImage(models.Model):
    blog = models.ForeignKey(BlogPost, on_delete=models.CASCADE)
    image = ImageField()


class BlogComment(models.Model):
    name = models.CharField(max_length=25)
    email = models.EmailField()
    comment = models.TextField()
    date = models.DateTimeField(auto_now=True)
    blog = models.ForeignKey(BlogPost, on_delete=models.CASCADE)


class ImageGallery(models.Model):
    title = models.CharField(max_length=50)
    image = ImageField()

    def __str__(self):
        return self.title

