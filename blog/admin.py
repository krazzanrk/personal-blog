from django.contrib import admin
from .models import *


# Register your models here.

class BlogHasImageTabularInline(admin.TabularInline):
    model = BlogHasImage


class ProductAdmin(admin.ModelAdmin):
    inlines = [BlogHasImageTabularInline]


admin.site.register(CategoryWiseBlog)
admin.site.register(ImageGallery)
admin.site.register(BlogComment)
admin.site.register(BlogPost, ProductAdmin)

