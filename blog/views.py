from django.shortcuts import render, redirect
from django.views import View
from .models import *
from .forms import CommentForm
from cms.models import Banner
from django.core.paginator import Paginator
from cms.models import Page


# Create your views here.
class BaseView(View):
    template_context = {
        # 'blogposts': BlogPost.objects.order_by('title').all(),


    }


class HomePage(BaseView):
    def get(self, request):
        self.template_context['banners'] = Banner.objects.filter(published=True).order_by('-weight')
        return render(request, 'index.html', self.template_context)


class BlogView(BaseView):
    def get(self, request):
        self.template_context['blogs'] = BlogPost.objects.all()
        self.template_context['top_posts'] = BlogPost.objects.order_by('?')[:4]

        blog_post_list = BlogPost.objects.all()
        paginator = Paginator(blog_post_list, 2)
        page = request.GET.get('page', 1)
        self.template_context['blog_pages'] = paginator.get_page(page)
        print(paginator.page_range)
        self.template_context['paginator'] = paginator
        return render(request, 'blog.html', self.template_context)


class SinglePostView(BaseView):
    def get(self, request, singleblog_slug):
        singlepost = BlogPost.objects.get(slug=singleblog_slug)
        self.template_context['single_posts'] = singlepost
        self.template_context['top_posts'] = BlogPost.objects.order_by('-id')[:4]
        self.template_context['next_prev'] = BlogPost.objects.order_by('?')[:1]
        self.template_context['comments'] = BlogComment.objects.all()
        return render(request, 'single-blog.html', self.template_context)

    def post(self, request, singleblog_slug):
        form = CommentForm(request.POST)
        if form.is_valid():
            print("success")
            blog_review = form.save(commit=False)
            blog_review.blog = BlogPost.objects.get(slug=singleblog_slug)
            blog_review.save()

        return redirect('/blog/' + singleblog_slug)

    # print("fail")
    # return redirect('/')


class SearchView(BaseView):
    def get(self, request):
        q = request.GET.get('q', None)
        if not q:
            return redirect('/')

        self.template_context['search_results'] = BlogPost.objects.filter(title__icontains=q)
        return render(request, 'search-result.html', self.template_context)


class GalleryView(BaseView):
    def get(self, request):
        self.template_context['galleries'] = ImageGallery.objects.order_by('?')
        return render(request, 'Imagegallery.html', self.template_context)
