from django.db import models
from sorl.thumbnail import ImageField
from autoslug import AutoSlugField
from ckeditor.fields import RichTextField


class Page(models.Model):
    title = models.CharField(max_length=50)
    menu_title = models.CharField(max_length=50)
    content = RichTextField()
    image = ImageField()
    published = models.BooleanField()
    slug = AutoSlugField(populate_from='title', unique=True)

    def __str__(self):
        return self.title


class Banner(models.Model):
    title = models.CharField(max_length=50)
    image = ImageField()
    published = models.BooleanField()
    content = RichTextField()
    weight = models.IntegerField()

    def __str__(self):
        return str(self.weight)
