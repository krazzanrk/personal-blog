from django.urls import path
from .views import *

app_name='cms'

urlpatterns=[
    path('about/',PageView.as_view(),name='abouts'),
    path('contact/',ContactView.as_view(),name='contacts')

]