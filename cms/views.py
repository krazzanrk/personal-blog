from django.shortcuts import render
from django.views import View
from blog.views import BaseView
from .models import *


# Create your views here.
class ContactView(BaseView):
    def get(self, request):
        return render(request, 'contact.html')


class PageView(BaseView):
    def get(self, request):
        self.template_context['abouts'] = Page.objects.all()
        return render(request, 'about-us.html', self.template_context)
